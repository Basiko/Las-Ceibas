jQuery(document).ready(function() {
	jQuery('.text').addClass("hiddens").viewportChecker({
	    classToAdd: 'visibles animated bounceInRight', // Class to add to the elements when they are visible
	    offset: 100
	   });
	jQuery('.img').addClass("hiddens").viewportChecker({
	    classToAdd: 'visibles animated bounceInLeft', // Class to add to the elements when they are visible
	    offset: 100
	   });
});
