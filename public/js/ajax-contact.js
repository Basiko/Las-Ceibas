$(function() {

	// Get the form.
	var form = $('#ajax-contact');

	// Get the messages div.
	var formMessages = $('#form-messages');

	// Set up an event listener for the contact form.
	$(form).submit(function(e) {
		// Stop the browser from submitting the form.
		e.preventDefault();

		// Serialize the form data.
		var formData = $(form).serialize();

		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
		})
		.done(function(response) {
			// Make sure that the formMessages div has the 'success' class.
			$(formMessages).removeClass('error');
			$(formMessages).addClass('success');

			// Set the message text.
			swal("Gracias!", "Nos pondremos en contacto contigo!", "success");

			// Clear the form.
			$('#name').val('');
			$('#mail').val('');
			$('#comment').val('');
			$('#phone').val('');
		})
		.fail(function(data) {
			// Make sure that the formMessages div has the 'error' class.

			// Set the message text.
			if (data.responseText !== '') {
				swal("Error!", 'Oops! un error a ocurrido y tu mensaje no pudo ser envidado', "error");
			} else {
				swal("Error!", "Oops! un error a ocurrido y tu mensaje no pudo ser envidado", "error");
			}
		});

	});

});
