$(document).ready(function(){

   var scroll_start = 0;
   var startchange = $('#navbar-change');
   var offset = startchange.offset();

   if (startchange.length){
   $(document).scroll(function() {
         scroll_start = $(this).scrollTop();
         if(scroll_start > offset.top) {
             $(".navbar-default").addClass('fadein');
          } else {
             $('.navbar-default').removeClass('fadein');
          }
      });
    }
});
